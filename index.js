const pdfGenerator = require('./lib/pdfGenerator');
const fs = require('fs');
const argv = require('./lib/argv');

const outputFile = argv.o;
const studentCount = argv.s;
const fontFile = argv.f;
const fontName = argv.n;

console.log(`Total students: ${studentCount}`);
console.log(`Output will be placed in ${outputFile}`);
console.log(`Font ${fontName} in file "${fontFile}" will be used`);

pdfGenerator({
  studentCount: studentCount,
  fontFilePath: fontFile,
  fontName: fontName,
  outputTarget: fs.createWriteStream(outputFile)
});
