const studentFirstNames = [
  '林', '白', '蘇', '楊', '李', '陳', '曾', '顏', '黃'
];

const studentLastNames = {
  middle: [
    '', '真', '子', '家', '安', '佳', '慧'
  ],
  third: [
    '婷', '琪', '惠', '欣', '萱', '彤', '玟'
  ]
};

const subjects = [
  '國文', '英文', '數學', '自然', '社會'
];

const scoreScheme = {
  rawScoreMax: 100,
  rawScoreMin: 0,
  scoreMappingCap: 85,
  mappedScoreMax: 15,
  mappedScoreMin: 1
};

function randomStudent() {
  return {
    firstName: randomPickOne(studentLastNames.middle) + randomPickOne(studentLastNames.third),
    lastName: randomPickOne(studentFirstNames),
    scores: subjects.reduce((acc, subject) => {
      acc[subject] = randomScore(scoreScheme);
      return acc;
    }, {})
  };
}

function randomPickOne(array) {
  const idx = Math.floor(Math.random() * array.length);
  return array[idx];
}

function randomScore(scoreScheme) {
  const rawScore = Math.random() * scoreScheme.rawScoreMax + scoreScheme.rawScoreMin;
  if (rawScore >= scoreScheme.scoreMappingCap) {
    return scoreScheme.mappedScoreMax;
  } else {
    const scoreDistance = scoreScheme.scoreMappingCap /  (scoreScheme.mappedScoreMax - scoreScheme.mappedScoreMin);
    return scoreScheme.mappedScoreMin + Math.floor(rawScore / scoreDistance);
  }
}

module.exports = {
  randomStudent: randomStudent,
  maxNameLength: 3,
  subjects: subjects.map(v => v) // export copy of subjects
};
