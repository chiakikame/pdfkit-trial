const Pdf = require('pdfkit');
const rs = require('./randomStudent');

const randomStudent = rs.randomStudent;
const rowGap = 4;

function pdfGenerator(param) {
  const fontFilePath = param.fontFilePath;
  const originalFontName = param.fontName;
  const outputTarget = param.outputTarget;
  const studentCount = param.studentCount;
  
  const pdf = new Pdf({size: 'A4', margin: 36});
  const mainFontName = 'main-font';
  pdf.registerFont(mainFontName, fontFilePath, originalFontName);
  pdf.font(mainFontName);
  pdf.fontSize(12);
  pdf.pipe(outputTarget);
  
  const margin = pdf.page.margin;
  
  // heading
  const pageLocationState = createState(pdf, rs.subjects);
  
  prepareForNewPage(pageLocationState);
  
  // content
  for(let i = 0; i < studentCount; i++) {
    const student = randomStudent();
    const studentFullName = student.lastName + student.firstName;
    console.log(studentFullName);
    drawStringInCell(pageLocationState, studentFullName, 0);
    
    rs.subjects.forEach((s, i) => {
      drawStringInCell(pageLocationState, student.scores[s].toString(), i + 1);
    });
    
    drawVertLines(pageLocationState);
    nextRow(pageLocationState);
    drawHoriLine(pageLocationState);
    
    if (shouldFlip(pageLocationState)) {
      console.log('add page');
      pdf.addPage();
      prepareForNewPage(pageLocationState);
    }
  }
  
  pdf.end();
}

function createState(pdf, subjects) {
  const clientWidth = pdf.page.width - pdf.page.margins.left - pdf.page.margins.right;
  const headingString = subjects.map(v => v);
  headingString.unshift('姓名');
  
  return {
    columnCount: (subjects.length + 1),
    rowGap: rowGap,
    columnWidth: clientWidth / (subjects.length + 1),
    rowHeight: pdf.currentLineHeight() + rowGap,
    leftmost: pdf.page.margins.left,
    rightmost: pdf.page.width - pdf.page.margins.right,
    currentY: pdf.page.margins.top,
    pdf: pdf,
    headingString: headingString
  };
}

function drawHoriLine(pageLocationState) {
  const pdf = pageLocationState.pdf;
  pdf.moveTo(pageLocationState.leftmost, pageLocationState.currentY)
    .lineTo(pageLocationState.rightmost, pageLocationState.currentY)
    .stroke();
}

function drawVertLines(pageLocationState) {
  const count = pageLocationState.headingString.length + 1;
  const pdf = pageLocationState.pdf;
  const x = pageLocationState.leftmost;
  const y = pageLocationState.currentY;
  const w = pageLocationState.columnWidth;
  const h = pageLocationState.rowHeight;
  for(let i = 0; i < count; i++) {
    pdf.moveTo(x + i * w, y)
      .lineTo(x + i * w, y + h)
      .stroke();
  }
}

function nextRow(pageLocationState) {
  pageLocationState.currentY += pageLocationState.rowHeight;
}

function drawStringInCell(pageLocationState, str, columnId) {
  const pdf = pageLocationState.pdf;
  const stringWidth = pdf.widthOfString(str);
  const x = pageLocationState.columnWidth * columnId + pageLocationState.leftmost + (pageLocationState.columnWidth - stringWidth) / 2;
  const y = pageLocationState.currentY + pageLocationState.rowGap / 2;
  
  pdf.text(str, x, y);
}

function shouldFlip(pageLocationState) {
  const pdf = pageLocationState.pdf;
  return pageLocationState.currentY + pageLocationState.rowHeight >= pdf.page.height - pdf.page.margins.bottom;
}

function prepareForNewPage(pageLocationState) {
  const pdf = pageLocationState.pdf;
  const headingString = pageLocationState.headingString;
  pageLocationState.leftmost = pdf.page.margins.left;
  pageLocationState.rightmost = pdf.page.width - pdf.page.margins.right;
  pageLocationState.currentY = pdf.page.margins.top;
  
  drawHoriLine(pageLocationState);
  headingString.forEach((s, i) => {
    drawStringInCell(pageLocationState, s, i);
  });
  drawVertLines(pageLocationState);
  nextRow(pageLocationState);
  drawHoriLine(pageLocationState);
}

module.exports = pdfGenerator;
