module.exports = require('yargs')
  // students
  .alias('s', 'students')
  .describe('s', 'Number of students')
  .nargs('s', 1)
  .default('s', 42)
  // output
  .alias('o', 'output')
  .describe('o', 'Output pdf file path')
  .nargs('o', 1)
  .default('o', './output.pdf')
  // font file
  .alias('f', 'fontfile')
  .describe('f', 'Font file with Traditional Chinese characters')
  .nargs('f', 1)
  // font name
  .alias('n', 'fontname')
  .describe('n', 'Name of the font you wish to use')
  .nargs('n', 1)
  // help
  .alias('h', 'help')
  .help('help')
  // finally
  .demandOption(['f', 'n'])
  .argv;
