# Pdf Grade Table Generator

A grade table generator. Output in PDF.

## Goal

Test capability of `pdfkit`, a pdf generation library in JavaScript, about the following aspects:

* Chinese text handling & font embedding
* Table generation
* Page switching

## Run

To run this application, you should supply a font with Traditional Chinese glyphs:

```sh
node index.js -f ~/tmp/wqy-microhei.ttc -n "WenQuanYiMicroHei" -o /tmp/output.pdf
```

or

```sh
npm start -- -f ~/tmp/wqy-microhei.ttc -n "WenQuanYiMicroHei" -o /tmp/output.pdf
```

## Result

All goals reached.

It may be handy to konw, if you want to know postscript name of font under Linux system, you may use:

```sh
fc-query fontfile.ttc | grep postscript
```
